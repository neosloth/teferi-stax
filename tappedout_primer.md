# Teferi Stax Primer: Jack of all Trades, Master of All.

## The most frequently asked question

**You don't need timetwister and tabernacle to play the deck. Run USZ over stroke if you don't have timetwister, run an island over tabernacle.**

===accordion

===panel:Introduction

## Deck Introduction

Chain Veil Teferi (CVT) is a bit of a unicum among its competitive peers. Not only can it race the fastest Combo decks in the format with frequent Turn 3-4 wins, it also has the ability to completely lock down the game until it can find its way to victory. Moreover, the deck is able to fluidly switch between the two strategies depending on the current game state. All of this makes CVT one of the stronger and more popular competitive Multiplayer EDH decks.

The two major upsides of CVT are that the deck is both resilient and consistent due to having one piece of its two-card Combo in the Command Zone. This, coupled with a slew of artifact tutors, lots of fast Mana,and a bunch of Counterspell backup can lead to what looks like a win "out of nowhere". While the Combo does need a fairly high amount of Mana to execute, Mana is almost never a concern once Teferi hits the field thanks to his -1 ability. Said -1 ability is also a big factor when it comes to playing the Stax game.

## Who We Are
### neosloth (superstepa)

Hi, I'm neosloth, also known as superstepa on reddit and tappedout.


If you hang around the r/cedh Discord you probably already know me as "that Teferi guy". I am a huge fan of mono blue strategies in cEDH, and I have always been trying to play the best mono blue general available. For the past year I have dedicated myself to making Teferi the best deck it can possible be. I have extensively tested my build of the deck against almost every tiered and fringe deck in the format, and I update the deck regularly in order to adjust to the new cards coming out and the shifts in the metagame. The deck has a proven record against some of the best r/cedh players, with one variant of this deck going undefeated in the 2017 Vancouver GP Competitive EDH Side Event, and a different variant of the deck took second place in the r/CompetitiveEDH's 5th Tournament. I believe that Teferi is, and has consistently been one of the best cEDH decks and hopefully this primer can show you why that is the case.

### Lobster (Neunviertel)

I'm Lobster, also known as SizOzzsome on Reddit, Neunviertel on TappedOut, or Sigi. Some of you may know me as the Admin of the PlayEDH Discord Server, or as the dedicated Mono Blue player on the [Laboratory Maniacs YouTube Channel](https://www.youtube.com/channel/UCo41YdkXVTp4dTXvhduaeYw). My first foray into competitive EDH was trying to tune up a Stax Zur list to no avail. Immediately afterwards, I picked up Teferi as my main deck because I always enjoyed playing Blue and just from looking at the list, I felt like it might be a good fit. I originally started with a list that was similar to Zrifts's Stax Teferi, but soon realized that it was out of date and not really fit for the Post-Partial Paris cEDH environment. This lead to me experimenting around and learning a lot about Teferi in the meantime.

## Pros and Cons

**Pros**

-   Can win on Turn 3-4 quite consistently
-   Can play hard Stax if need be
-   Plays the long game much better than other fast decks
-   Can switch between fast Combo and hard Stax very quickly
-   Runs a lot of interaction, both proactive and reactive
-   High consistency thanks to many different tutor lines

**Cons**

-   Has trouble dealing with Null Rod/Stony Silence
-   Doesn't like Sphere of Resistance/Thalia, Guardian of Thraben type effects
-   Is (sublty) hard to play and build correctly

## Why play this deck?

**You might enjoy this deck if you:**

-   Really like Blue
-   Enjoy playing with lots of artifacts
-   Like to be flexible in your game plan
-   Like having half of your winning combo in the Command Zone
-   Enjoy a deck that has a lot of subtle depth, but a well-defined overall goal

**You will not enjoy this deck if you:**:

-   Hate Blue
-   Don't like the idea of only having access to one colour
-   Prefer decks that try to execute the same plan every game
-   Prefer finishing the game differently every time
-   Prefer creature-based decks
-   Don't want to interact with opposing decks
-   Want to play with [[card:Time Warp]] and [[card:Consecrated Sphinx]]

## Deck Tech

<iframe width="560" height="315" src="https://www.youtube.com/embed/7RvU7yFAAzk" frameborder="0" allowfullscreen></iframe>
===endpanel



===panel:Gameplan

# Gameplan

Before we begin talking about the combo itself, here is the relevant ruling. Yes, it is pretty much the opposite of what the card itself says, but it's how the combo works. When we recast Teferi from the CZ, he gets all the previous activations of [[card:The Chain Veil]], allowing him to make more mana and draw more cards each iteration.

> 7/18/2014
>
> Because the last ability modifies the rules of the game, it affects not only planeswalkers you control when it resolves, but also planeswalkers that come under your control later in the turn.

Here is the basic gist of the combo. You cast Teferi, untap 4 things, cast chain veil, untap chain veil. Now, you untap 4 things with one of them being chain veil. If you have enough mana rocks (5 mana from three permanents), you will net a little bit of mana each time you activate veil. On the last Teferi activation you simply untap 4 things that net you the most mana and leave The Chain Veil tapped. Now, we recast Teferi with the mana we generated from the loop. Because this Teferi is a new game object he gets the number of free activations equal to the number of times you used The Chain Veil. So if you used it 3 times, you can instantly -3 to generate a whole bunch of mana before resuming the combo. You can also +1 first, and then tick Teferi down essentially repeating the first loop you did. You'd want to do that if there is something like a Ruric Thar on the battlefield and you need to dig for answers.

Once the loop gets going you are eventually going to have practically infinite mana and infinite planeswalker activations. With those you can now use Teferi's +1 to dig through your whole deck. Then you find [[card:Ugin, The Spirit Dragon]] and use his +2 on every opponent until they are dead. If your Ugin is somehow exiled you can just dig for [[card:Stroke of Genius]] or any other infinite mana outlet.

Before attempting to combo off, always make sure that you have enough mana to actually win. If you all you have is a [[card:Sol Ring]] and some tapped lands, chances are you won't have enough mana to get the loop going. There are edge cases where you might be able to win without enough mana rocks. All you have to do is get veil to 5 stacked activations. To make life a bit easier for you, we made a small Python script that will tell you if you're able to go off. You can find it [here](https://github.com/Neunviertel/teferi-combo-checker).

**The rule of thumb for the chain veil combo is: 5 mana from 3 permanents, or 4 mana from 3 permanents and 12 mana avaliable before Teferi. Generally speaking, if you can recast teferi you win the game. You can see the proof for 4 mana, 3 permanents determinancy [here](https://www.reddit.com/r/CompetitiveEDH/comments/8buagu/proof_that_the_teferi_chain_veil_combo_works_with/)**

Now that we're all on the same page when it comes to how to execute our main win condition, let's talk about the one thing that sets Teferi apart from other decks. Not only can Teferi be a Fast Combo-type deck, it can also play the Stax role. More importantly, it can switch between these two "modes" very quickly and seamlessly, which is one of the reasons why it's such a powerhouse in competitive EDH. This, however, is also the reason why Teferi is a deck that has a lot of subtle depth to it: Deciding on the correct gameplan and knowing when and how you're supposed to change your role at the table is very complicated and takes both a skilled pilot and a high amount of experience with the deck. The following segment will attempt to illuminate the thought process behind formulating the correct gameplan in regards to three different factors:

1. Opening Hand
2. Other Decks at The Table
3. Position at the Table.

## Opening Hand

Before we talk about which gameplan we want to decide on, here's a short bit on mulligan decisions. Generally speaking, our ideal hand will look similar to this: 2-3 lands, 1 piece of interaction, 2-3 pieces of ramp, 1-2 draw spells, 1 tutor. While Mulligan decisions definitely go hand in hand with our gameplan, there are certain hand configurations that we really don't want to see:


1. Too many lands: This one applies to pretty much every cEDH deck out there. Even when going for a long game, you don't want to keep a hand that is too heavy on lands and too light on gas.
2. 0-1 lands: There are very, very few situations where keeping a no-lander is worth it, so this should almost always be shipped back. One land is keepable if your hand has a decent balance of ramp and draw.
3. Very slow hands: If our hand is several pieces of interaction, a tiny bit of ramp, and no real way to draw/tutor cards, it should go back. Especially in slow games, we want to have the ability to see more cards than our opponents, so just keeping interaction is not the way to go.
4. All ramp hands: If we've got a hand where we can have like 10-12 mana by turn 3, but without any real payoff, we should mulligan. Although this deck plays a lot of fast Mana, it will quickly look weak when there's no payoff for that Mana.
5. All-in hands: These can be kept against the certain decks, but most times, you should expect early interaction from your opponents. If our hand relies on one key piece too much, we can fall very far behind if that particular piece gets interacted with. Teferi is not a deck that needs to take this sort of risk.

After we've decided to keep our opening hand, the main criterion of evaluation should be the hand's speed. In this case, speed doesn't necessarily mean how fast the deck is going to win with this hand if uninterrupted, but rather how quickly it can do two things:

1. How quickly can we establish a strong board presence?
2. How quickly can we see how many cards in our deck?

A strong board presence can mean a lot of things in this deck, but it basically boils down to having access to more Mana than our opponents. This can be achieved in a variety of ways: Having a lot of cheap Mana rocks, landing a very fast [[card:Stasis]] with Teferi out, or even shutting off our opponents' access to Mana through cards like [[card:Back to Basics]], [[card:Winter Orb]], [[card:Static Orb]], or [[card:Cursed Totem]].


After establishing our board, we have to do something with it, and that's where the other aspect of speed comes in. We don't just want to have an advantageous board state, we want to do something with it, and to put it simply: more cards means more things we can do. And the quicker we can do more things, the better.


These two aspects of speed go hand in hand. We don't just want to have one without the other. If all our hand does is getting on the board quickly, but without a way to go from there, it's not going to go well for us. This hand could be described as one that has a lot of speed in the early stage of the game, but once the board is established, its speed will drop massively, as visualised in [this graph](http://i.imgur.com/uPhz3MM.png). On the flipside, if our hand would let us see a lot of cards, but we'd have to use most of those cards to make our board presence workable, we're wasting a lot of resources that could've been used to propel us towards victory. Ultimately, this type of hand is a bit better than the first type since it can speed up as the game goes along (which makes its graph look [something like this](http://i.imgur.com/JYmjUfn.png)), but when taking this approach, we're usually left vulnerable to an opponent comboing out without us being able to do much about it, or to interaction that's aimed at one of our key cards. The major issue with keeping these hands is that we often end up constrained on Mana in the early game, since all of it has to go into trying to develop our board with the pieces we find through our draw spells.


Going back to our two different possible gameplans, here are some graphs for the stronger types of hands we usually encounter:


### The Typical Fast Combo Hand
![The typical fast combo hand](http://i.imgur.com/GegMRdU.png)


For this hand, we want to go hard and fast. If we see a bunch of Ramp that will give us enough Mana to cast Teferi early, a way to find The Chain Veil quickly, and maybe a Counterspell to protect our combo, this is where we want to be. A good example would be Island, Island, [[card:Mana Crypt]], [[card:Mox Opal]], [[card:Grim Monolith]], [[card:Transmute Artifact]], [[card:Flusterstorm]]. We have access to 8 Mana on Turn 2, which may already be enough to win the game.


### The Slow, build-up, interaction-heavy hand
![The slow, build-up, interaction-heavy hand](http://i.imgur.com/3Hm7sxn.png)


If we've got a bit of ramp and some repeatable card draw coupled with good pieces of interaction, we can usually try to grind our way towards a victory by accumulating card advantage and going off once we've managed to build up enough of a board state. This is basically the better version of our second negative example (Graph 2). A sample hand would be [[card:Scalding Tarn]], [[card:Polluted Delta]],[[card:Jace, Vryn's Prodigy]] , [[card:Mystic Remora]], [[card:Mana Crypt]], [[card:Mana Drain]], [[card:Back to Basics]]. With this hand, we can drop an early [[card:Mystic Remora]] and a JVP, and back it up with [[card:Back to Basics]]. This will give us a lot of card advantage and selection, which should mean that we pick up more and more speed over the course of the game.


### The fast-lockdown hand
![The fast-lockdown hand](http://i.imgur.com/XttFiOD.png)


This is what would typically be considered a Stax hand. We get a good board presence going upfront before we drop a strong piece of lockdown (think [[card:Winter Orb]], [[card:Static Orb]], [[card:Stasis]]). While this will also slow us down to some degree (albeit less than other decks), it will buy us time to find the remaining pieces we need to close out the game. Once we have those assembled, we can usually go off uninterrupted. A prime example for this type of hand would be the following: [[card:Island]], [[card:Mishra's Workshop]], [[card:Mox Opal]], [[card:Sol Ring]], [[card:Grim Monolith]], [[card:Cursed Totem]], [[card:Static Orb]]. Here, we can come out of the gates really quickly with a bunch of Mana rocks, which we can then follow up with [[card:Cursed Totem]] to stop Mana dorks, and Teferi + [[card:Static Orb]] to keep the Mana advantage over our opponents. Teferi also helps us go through parts of our deck at a moderate pace so we can keep interacting with our opponents while advancing our own board state.


Apart from speed, the other main factor to consider when it comes to evaluating opening hands is payoffs. These payoffs don't necessarily have to be game-winning plays, they can also be something like resolving a [[card:Dig Through Time]], getting a lot of cards off [[card:Mystic Remora]], or locking down the table with Teferi + [[card:Stasis]]. One thing to keep in mind when looking at a hand is identifying these payoffs. If our hand has no proper payoffs, we may find ourselves in a Graph 1 situation, where we can do a bunch of stuff upfront but run out of gas pretty quickly. We always want our hands to go somewhere


To sum it up: Going slow is fine. More than anything, your hand needs to have a clear goal.

## Matchups

The second big factor when it comes to deciding on our initial gameplan is matchups. After taking a good look around the table, we should try to identify our opponents' decks and consider their possible gameplans. When doing this, most cEDH decks can usually be classified by two categories: speed and interactivity. Speed, in this case, means how early in the game a deck can usually threaten to win. Interactivity not only correlates with the amount of interaction a deck runs, but also it's willingness to use this interaction to prolong the game. As a guideline for how to evaluate some of the more popular decks in the format by these two attributes, we've prepared another graph.

### cEDH Decks By Speed and Interaction
![Cedh Decks By Speed and Interaction](https://i.imgur.com/77VgdZQ.png)

What's quite apparent from looking at this graph is that there's usually an inverse relation between a deck's speed and its Interactivity. Another conspicuity is that most decks in the format tend to gravitate towards one end of the spectrum. They're either trying to be fast without interacting much, or they're trying to interact a lot, which makes them slower in nature. Since one of Teferi's biggest strength is its flexibility in terms of gameplan, we can abuse the nature of these other decks by adjusting our gameplan to the matchups. Teferi is neither the fastest nor the most interactive deck in the format, and it really shouldn't be trying to be either one of those if that would mean losing strength in the other direction. If we're up against a table that is mostly comprised of interactive decks, we can play the role of the fast combo deck that's threatening an early win. If we're not the fastest deck at the table, we can look to slow down the game and find the right window to win. If it's a mix between fast and interactive decks, we can go for a middle-of-the-road kinda thing where we watch out for the faster decks while not letting the slower decks get enough of a grip on the game. These types of pods usually take a fair amount of experience because they can be very unintuitive to navigate successfully. Doing so usually boils down to being patient and thinking ahead far enough to make the right choices about which cards to interact with. Sometimes, that also means taking a step back and letting other decks interact while slowly advancing our own board state.

## Turn Order

Apart from the innate advantage of going first in a game, which you can read about [here](https://en.wikipedia.org/wiki/First-move_advantage_in_chess) (Note that this advantage naturally isn't as big in Multiplayer games. However, there is a significant disadvantage to going last that is unique to multiplayer formats), there are some other things to consider about Turn order. A lot of this ties into our previous segment: We should look at how the different types of decks are positioned towards each other. Does the fast Combo deck untap before or after the slow, interactive deck? Who will usually have untapped Mana when it's our turn? Do we maybe need to stop ramping and start interacting a turn earlier because of how the turn order is set up? Could it be the other way round, so we have more time to build up our board, and can we abuse that somehow? These are all questions we should ask ourselves before the game starts. The main way in which they shape our gameplan can be summed up by evaluating how greedy we can allow ourselves to be, or how conservatively we have to play.

The big thing to keep in mind about all of these factors is that they don't just stand by themselves. They all interweave and play off each other, and our final overall gameplan should take all of them into consideration.

## Reacting to the Board

As we've alluded to previously, Teferi's biggest strength lies in its flexibility. That much is not only true when it comes to deciding on our initial gameplan, but also when it comes to, as some people say, "playing to the board". Over the course of a game, we should always evaluate the board state. This means that we should identify who the most threatening deck at the table is at any given point. We can also go one step further and consider which deck will gain the most from having the threatening deck be dealt with (aka "the last deck to try to combo off usually wins"). On the other hand, if we're the most threatening deck at any given point, we should pay special attention to the deck that has the highest potential to disrupt our plans and change our gameplan to deal with them accordingly.

## Scenarios

We'll start collecting some actual games and our thoughts about them in relation to everything above, which will be added to this section in the future.


===endpanel



===panel:Single Card Discussion

Every deck should be tuned to its metagame, and because of that this SCD section is not matched to a specific Teferi list. The following cards are the cards that we think are the core of every good Teferi deck, but these do not collectively form a specific Teferi list. If you want an example of a Teferi list, please scroll down to the bottom of the primer.

===accordion
===panel:Ramp

The Daily Bread of every competitive Teferi deck, copious amounts of Mana Rocks provide us with enough acceleration to power out our Commander very early if need be. For Teferi, a suitable Mana Rock should:

1.  Be cost-efficient: This includes free Rocks (Mana Crypt, Mox Diamond), Mana-positive Rocks (Mana Vault, Sol Ring), or just the better 2-cmc Rocks (Fellwar Stone, Mind Stone).

2.  tap for 3 Mana: When going for your Chain Veil combo, you don't need any other 2+ Mana sources if you have a single Source that taps for 3 Mana. All cards in this section will be evaluated according the critera listed above.

### Ramp Card List:

- [[card:Basalt Monolith]] - Pays for itself and goes infinite with [[card: Power Artifact]] and [[card: Rings of Brighthearth]].

- [[card:Chrome Mox]] - Taps for blue for 0 mana.

- [[card:Copy Artifact]] - Copies the best ramp piece at the table.

- [[card:Fellwar Stone]] - 2 mana and it taps for a colored. It's the best we can get without talismans.

- [[card:Gilded Lotus]] - Taps for colored mana so we can easily keep up a counterspell after casting it.

- [[card:Grim Monolith]] - 2 mana and it taps for 3. Goes infinite with [[card: Power Artifact]].

- [[card:Sol Ring]] - The reason commander decks are 98 cards.

- [[card:Mana Crypt]] - The reason budgetless commander decks are 97 cards.

- [[card:Mana Vault]] - It's a dark ritual that we can untap with Teferi.

- [[card:Mind Stone]] - One of the better 2 mana tap for 1 rocks. Can be cycled late game.

- [[card:Mox Diamond]] - Taps for blue for 0 mana.

- [[card:Mox Opal]] - It's the best mox. With the amount of artifacts we have in our deck this is pretty much always going to be active.

- [[card:Prismatic Lens]] - The second best 2 mana tap for 1 rock. The colored mana filtering can be surprisingly relevant.

- [[card:Thran Dynamo]] - It's just good enough to make the cut. Having rocks that tap for 3 mana is important because they allow us to combo out with the chain veil.

- [[card:Voltaic Key]] - Good with pretty much every mana positive rock. Lets us untap monoliths to net 5 mana.

===endpanel

===panel:Interaction

Since this deck doesn't just play Fast Combo, but also needs to go for the long game from time to time, we run a more extensive interactive suite than most competitive EDH decks. The interaction we run usually boils down to one of three categories:

1.  **Protection**: This includes interaction on the Stack in the form of Counterspells to either protect our board against opposing interaction or prevent another player from winning.

2.  **Removal**: These spells should be used to clear out any obstacles that are getting in our way.

3.  **Stax/Hate**: The cards that make it possible for us to play (and win) the long game. Even when up against other Stax decks, controlling what affects the board is a strong position to be in.

===endpanel

===panel:Protection

- [[card:Counterspell]] - UU to stop anything.

- [[card:Delay]] - Often acts as a hard counter for 1U, stopping enemy generals, counterspells and removal.

- [[card:Dispel]] - Dispel's main function is protecting our combo turn. The card can also hit relevant instants such as [[card:Ad Nauseam]] when necessary.

- [[card:Flusterstorm]] - Another excellent protection spell. In addition to being very hard to counter, this card can destroy opposing storm decks.

- [[card:Force of Will]] - Free counterspell. We are a mono blue deck so we'll always have something to pitch to it.

- [[card:Mana Drain]] - Strictly better counterspell (not a statement you hear often). Can help us combo out early.

- [[card:Mental Misstep]] - Stops enemy ramp (sol rings and elvish mystics), tutors, removal and counterspells. Amazing card in a competitive meta, but a subpar one in a slower meta.

- [[card:Muddle the Mixture]] - Mostly used as a tutor for Transmute Artifact and Stasis, however it can act as protection in a pinch.

- [[card:Negate]] - Most of the "must answer" cards in cEDH are non-creature, so this will almost always be an unconditional counterspell for 1U. The main downside here is that it cannot answer generals.

- [[card:Pact Of Negation]] -  Mostly used to protect our combo but can be thrown out to stop an opponent from winning.

- [[card:Spell Pierce]] - Best mana leak effect in cEDH. A lot of cards relevant to us (hello, [[card:Null Rod]]) are non-creature and a lot of the time we can catch our opponents off guard with a well placed pierce.

- [[card:Swan Song]] - Best 1 mana counterspell. The 2/2 can be annoying when Teferi is on the battlefield, but it's not a concern most of the time.

===endpanel

===panel:Removal

- [[card:Chain of Vapor]] - Chain of Vapor is one of most deceptively powerful blue cards. While on a surface it may seem like a pretty straighforward bounce spell with a downside, an experienced player can turn it into one of the most potent removal spells in the entire format. Being able to read the table can help you put your opponents in a position where they are willing to copy chain of vapor bouncing someone else's card, essentially becoming a 2 for 1. The most important part of that, however, is that chain can be used to bounce your own permanents. The simpliest way to use it would be to generate mana off our fast mana rocks. Sometimes sacrificing a land and bouncing a mana vault and a mana crypt is enough to get us that little bit of extra mana for a win.

- [[card:Cyclonic Rift]] - This is spot removal and a wrath in the same card. The overload is not relevant in most games, but it can easily turn games that should've been a loss into a win.

- [[card:Into The Roil]] - Being able to bounce our own permanents is huge in a stax based deck like Teferi, because we can use it to get extra untaps through stasis and the orbs. Being able to cycle this lategame to dig deeper is pretty nice too.

===endpanel

===panel:Stax

- [[card:Cursed Totem]] - Stops creature decks. Remember that it also stops mana abilities, so it seriously hurts decks that use dorks as their ramp.

- [[card:Grafdigger's Cage]] - Stops reanimator decks and some storm decks. Note that it hurts our snapcaster mage and Jace.


- [[card:Back to Basics]] - Our answer to greedy 3+ color decks. A well timed [[card:Back to Basics]] can knock a deck out of the game entirely.

- [[card:Rhystic Study]] - Continuous tax effect. Doesn't matter whether they pay 1 or not, both are equally beneficial to our gameplan.

- [[card:Stasis]] - Often a game over with Teferi on the battlefield. In a worst case scenario it can act as a pseudo timewalk. Be mindful of our U producing mana sources when trying to cast it.

- [[card:The Tabernacle at Pendrell Vale]] - Our best stax piece against creature decks. Absolutely brutal when combined with [[card:Winter Orb]].


===endpanel

===panel:Card Draw

The butter to our bread, card draw makes it easier for us to get to our win condition. As a rule of thumb, the best card draw spells are those that let us see the most cards for the smallest cost. As with Mana Rocks, all cards in this section will be evaluated according to these two criteria.

- [[card:Brainstorm]] - Digs us 3 cards deep and interacts favorable with fetchlands.

- [[card:Dig Through Time]] - Digging 7 cards deep will often give us the exact card we are looking for.

- [[card:Fact or Fiction]] - Digs 5 cards deep and pretty much always generates card advantage.

- [[card:Frantic Search]] - While it's not technically card advantage, digging two cards deep for free is *really* good. Interacts favourably with high tide.

- [[card:Impulse]] - Digs 4 cards deep at instant speed.

- [[card:Manifold Insights]] - This card gets a slot through just its sheer efficiency. While you will almost never get a winning line through this, drawing 3 interactions spells will almost always put you in a favorable position. There is also a political element to it where a player might give you a stax piece that affects the other two decks at the table.

- [[card:Mystic Remora]] - Not really a stax effect, it's more akin to a mass draw effect. Most decks can't afford to pay 4 for their non creature spells, and most competitive decks can't afford to do nothing in the early development turns. In any case, it's often an ancestral recall and ancestral recall is a fantastic card.

- [[card:Sensei's Divining Top]] - Top filters out draws, is tutorable with trinket mage and it draws us the deck with [[card:Rings of Brighthearth]] and infinite mana.

- [[card:Memory Jar]] - It's a wheel effect that is extraordinarily easy to tutor for. We can also get it out early, keeping it up for combo turns or disruption on your opponent's combo turns.

- [[card:Ponder]] - Potentially digs us four cards deep. Interacts favorably with fetchlands.

- [[card:Preordain]] - Provides the best card selection out of all our cantrips.

- [[card:Sleight of Hand]] - simply the next best cantrip we have access to.

- [[card:Timetwister]] - Great for refilling our hand. The shuffle clause can be occasionaly relevant. If we lose our Jace, or we need to loop our [[card:Stroke Of Genius]], this is the card to get.

- [[card:Time Spiral]] - Pretty much just a second Timetwister (that is $965 cheaper). The untaps interact nicely with high tide and lands that tap for more than one mana.

- [[card:Thirst for Knowledge]] - Draw three discard two at instant speed is already pretty decent, and with this deck we'll always have something to pitch.

- [[card:Windfall]] - Great for refilling our hand. Can sometimes be used to punish aggressive tutors. For example, if you see a Sidisi ANT player tutor the turn before you can use the Windfall to make them discard Ad Nauseum, essentially knocking one player out of the game.

===endpanel

===panel:Tutors

Outside of packing the deck with every possible playable artifact tutor we can find, we also want to have access to specific key cards in our deck. F.i.,  if we need to play the long game, but can afford to play Teferi and let him sit for a while, we want to gain access to Stasis as quickly as possible.



- [[card:Fabricate]] - Gets us any artifact for 2U. Usually it's going to be [[card:The Chain Veil]] but you can always grab ramp or stax pieces.

- [[card:Muddle the Mixture]] - Usually gets [[card:Stasis]] or [[card:Transmute artifact]]. Other notable targets are [[card:Merchant Scroll]], [[card:Mana Drain]], [[card:Into the Roil]] and [[card:Grim Monolith]].

- [[card:Intuition]] - Gets us the card we want. See the panel below for notable piles.

- [[card:Inventors' Fair]] - This is an artifact tutor on a land. The lifegain is rarely relevant but it's nice to have. Gives the deck some extra lines to take when [[card:Expedition Map]] is played.

- [[card:Mystical Tutor]] - Gets pretty much everything. [[card:Timetwister]] and [[card:Transmute Artifact]] are the two common targets.

- [[card:Reshape]] - The worst of our artifact tutors. Paying 4UU to get the veil is not ideal, but it definitely gets us there.

- [[card:Tezzeret the Seeker]] - This is the best cEDH planeswalker ever printed. He gets us veil, mana ramp when needed, and he can act as additional ramp with his +1. He also makes comboing off with veil easier because we can use his untap as well as Teferi's to generate 4 mana off a sol ring.

- [[card:Transmute Artifact]] - This card is the best artifact tutor. Note that you have to pay the difference on resolution and not when casting the spell, so this can lead to some mind games with our opponents.

- [[card:Trinket Mage]] - Can get [[card:Chrome Mox]], [[card:Mana Crypt]], [[card:Mana Vault]], [[card:Mox Diamond]], [[card:Mox Opal]], [[card:Sol Ring]], [[card:Voltaic Key]], [[card:Seat of the Synod]], [[card:Sensei's Divining Top]] and [[card:Expedition Map]]. He also gives us the silly line of [[card:Expedition Map]] -> [[card:Inventors' Fair]] -> [[card:The Chain Veil]].

- [[card:Merchant Scroll]] - While the target largely depends on the context, [[card:Whir Of Invention]] a removal spell, a counterspell or [[card:High Tide]] are usually the cards to get.

- [[card:Spellseeker]] - Tutors a large variety of game winning cards including [[card:Transmute Artifact]], [[card:High Tide]] and [[card:Cyclonic Rift]]. Can also tutor up interaction in dire scenarios

- [[card:Whir of Invention]] - If not for the additional U, this card would be a strictly better improvement over the subpar [[card:Reshape]]. Note that we can deactivate our orbs by using them to pay for improvise.

## Intuition Piles

- [[card:Timetwister]], [[card:Windfall]], [[card:Time Spiral]] if you need to refill your hand
- [[card:Transmute Artifact]], [[card:Tezzeret the Seeker]], [[card:Whir of Invention]] if all you need is an artifact tutor
- [[card:Dispel]], [[card:Force of Will]], [[card:Pact of Negation]] for guaranteed protection on the combo turn
- anything+[[card:The Chain Veil]] when we have [[card:Buried Ruin]] or [[card:Academy Ruins]].
- [[card:Academy Ruins]] + 2 artifact stax pieces to lock down the game.
- [[card:Basalt Monolith]] + [[card:Mana Vault]] + [[card:Grim Monolith]] to get enough mana to kickstart the combo.
- [[card:Jace, Vryn's Prodigy]] + [[card:Rhystic Study]] + [[card:Mystic Remora]] for a guaranteed draw engine.

Please keep it in mind that going for a "guaranteed" Intuition pile is often a wrong play to make. What you get is dependent on your opponents, hand and boardstate and once you gain a basic understanding of the deck it will become a lot easier to throw together Intuition piles on the fly. For general advice on how to play Intuition we recommend the fantastic [Intuition primer](https://www.reddit.com/r/CompetitiveEDH/comments/5sbrkz/scdprimer_how_to_trust_your_intuition/) by /u/Lime_Blue.

===endpanel

===panel:Combo

These cards either set us up with an avenue towards victory, or can be used to win the game on the spot. When it comes to evaluating these types of cards, we should always think about how useful they are going to be outside of their Combo, since we want to minimize dead draws as much as possible.

- [[card:High Tide]] - Nets us a ludicrous amount of mana with Teferi. Tutoring high tide is often a valid line to take when you have TCV and not enough artifact mana.

- [[card:Ugin, the Spirit Dragon]] - The deck could theoretically run any planeswalker with a game ending ability as the win outlet, with [[card:Jace, The Mindsculptor]] being the most notable Ugin alternative, but Ugin is the win condition you will see in most Teferi decks. In the current creature heavy meta between JTMS and Ugin, Ugin has more utility. Reaching 8 colorless mana can be trivial with Teferi, and having a conditional boardwipe is backbreaking against most decks. Casting a teferi into Ugin's -5 is often game winning.

- [[card:The Chain Veil]] - The one and only. Getting this card is your goal, and once it lands the game is over.

- [[card:Rings of Brighthearth]] - In addition to comboing with [[card:Basalt Monolith]] this card favorably interacts with our general (you can copy either one of the abilities) and fetchlands.

- [[card:Power Artifact]] - Goes infinite with monoliths. One notable interaction this card has is enchanting [[card:The Chain Veil]]. The ability will cost 2, and untapping islands will generate 3 mana, so you'll be able to go infinite without any mana rocks.

- [[card:Stroke Of Genius]] - This is our infinite mana outlet of choice. This deck runs it over USZ because it's colored cost is easier to achieve when we cast stroke for value. The downside of stroke, however, is that we need to have spell recursion in order to kill the whole table with it. If we have somehow used up our timetwister, time spiral, and snapcaster or jace then it'll be impossible for we to win. The card is mostly interchangeable with USZ so pick the one that matches your deck better.

===endpanel

===panel:Lands

The basic questions we should ask ourselves when evaluating Land choices for Teferi are the following:

1. When is this Land going to be better than a basic Island?
2. What makes it better than a basic Island in our deck?
3. How often is that going to be the case?
4. For utility lands: Which parts of our deck does this land interact with?

If a land is frequently better than an Island, and it synergises with other cards in our deck well, then it's probably worth including.


- [[card:Island]] - Pretty self-explanatory. This card has been the bane of competitive MTG since it's inception and some people are arguing that it should perhaps be banned.

- [[card:Academy Ruins]] - Repeatable artifact recursion on a land. Gives us extra intuition lines.

- [[card:Ancient Tomb]] - Taps for 2 mana. Interacts nicely with Teferi, just watch your health when you are trying to combo out with it.

- [[card:Buried Ruin]] - Artifact recursion on a land. Gives us extra intuition lines.

- [[card:Flooded Strand]], [[card:Misty Rainforest]], [[card:Polluted Delta]], [[card:Scalding Tarn]] - These are just here for the shuffle on demand effect. Good with our cantrips and [[card:Sensei's Divining Top]]

===endpanel

===panel:Currently Testing

The cards in this section are the cards that were recently added to the core list. After playing some test games with these cards they will either be permanently moved to the core list, or moved to a more appropriate category such as Flex Slots.

## Card Draw

###Pull From Tomorrow

[[card:Pull From Tomorrow]] is one of the most efficient instant speed card draw spells in the format. In addition to just being a really solid value cast, this card can also be used as an infinite mana outlet. Unfortunately it does not target so it cannot be used as a win condition, so it would have to be ran alongside [[card:Stroke of Genius]] in builds that want an alternate win condition.

===endpanel

===panel:Flex Slots

The cards in this section are the cards that fill meta-specific niches. While these are all extremely strong cards, they might not belong to the core list for one of the following reasons:

- The card is dead in some matchups. For example, cards like [[card:Negate]] are largely useless against all-in creature decks.
- The card is dependent on a card that can be either added, or removed from your current list. For example, if you play in a meta that does not require you to run [[card:The Tabernacle at Pendrell Vale]], then perhaps, [[card:Expedition Map]] does not warrant a slot either.


### Ramp
- [[card:Lotus Petal]] - If you are trying to go for a more fast-combo oriented build, then a [[card:Lotus Petal]] could get you a little extra boost in speed. The downside of petal is that it opens you to easy 2-for-1s when something you use a Petal on gets countered or removed. The petal also doesn't interact all that favorably with all our artifact synergies, as it does not stick around for metalcraft and it cannot be untapped with Teferi.

### Removal

- [[card:Pongify]] - Can slow down commander focused decks like [[cards:Selvala, Heart of the Wilds]]

### Protection

- [[card:Mana Leak]] - Can catch people off-guard in fast games with thin mana margins.

- [[card:Steel Sabotage]] - Can be worth running if your meta is null rod heavy. It can also be used a pseudo ritual with mana positive mana rocks, and it can be used to break stax parity by bouncing your own winter and static orbs.


### Stax

- [[card:Torpor Orb]] - Stops creature based combo decks and creature based artifact removal. Most notably it shuts down Food Chain Tazri.

- [[card:Tangle Wire]] - Tangle Wire is fantastic against decks that are light on permanents. If you are playing against a lot of storm, then feel free to slot this in.

- [[card:Tormod's Crypt]] - Grave hate that does not cost any mana, does not exile our graveyard and is tutorable with [[card:Trinket Mage]].

- [[card:Winter Orb]] - Shuts down entire decks. Acts as a pseudo-boardwipe when combined with [[card:The Tabernacle at Pendrell Vale]]. Be mindful of decks that heavily rely on artifact and creature ramp, as those decks may have no trouble playing the game through it.

### Tutors

- [[card:Expedition Map]] - Gets us to veil with [[card:Inventors' Fair]], gets us ramp with [[card:Ancient Tomb]], and gets us creature hate with [[card:The Tabernacle at Pendrell Vale]]. Not worth it unless you run all of those. If you have an absurd amount of mana you can trinket mage it out to win the game.

### Lands
- [[card:Strip Mine]] - Great if you are either running the [[card:Expedition Map]] package, or if [[card:Gaea's Cradle]] is a problem in your meta.

- [[card:Gemstone Caverns]] - It's an occasional chrome mox, but having a colorless land when you are trying to tide out can be really rough. It's good in builds that plan to go the fast combo route.

### Misc

- [[card:Gilded Drake]] is the best mind control effect in the format. While it does not have the artifact synergies [[card:Vedalken Shackles]] has, it is harder to interact with and it is a lot cheaper to use.

- [[card:Snapcaster Mage]] - Lets us recast our spells and gives us extra intuition lines. The main utility of snapcaster mage is winning the game after infinite mana and infinite draw with ugin or the chain veil exiled. Casting [[card:Stroke of Genius]], casting time spiral and recasting it, and then using Snapcaster Mage to recast it can kill 3 players. Note that this can also be accompilished by casting Timetwister or replacing Stroke of Genius with Blue Sun's Zenith.

- [[card:Vedalken Shackles]] - If you're facing a lot of decks that base their main strategy on specific creatures (think New Selvala, Yisan), look no further. Post tuck change, [[card:Treachery]]-type effects are pretty much the only means of keeping some decks from having access to their commanders, and Shackles is the most efficient and flexible one we have. Even if you can't immediately steal your opponent's centrepiece, stealing a mana dork can give you a significant swing in Tempo.

- [[card:Jace, Vryn's Prodigy]] - Loots us in the early game, lets us recast our spells in the late game. We can use the flashback ability for things like double high tide, or you could give a counterspell flashback just in case you are expecting disruption. The main problem with it is that it gets shut down by the two stax pieces we tutor out the most often: [[card:Grafdigger's Cage]] and [[card:Cursed Totem]]
### Combo

- [[card: Jace, the Mind Sculptor]], often regarded as the best Planeswalker ever printed, is an absolute powerhouse in Legacy and Vintage. And for good reason - even in EDH, he kinda does it all: his +1 destroys top-of-deck tutors, his 0 lets us cast a free Brainstorm every turn cycle, his -1 bounces problem creatures back to their owners' hands, and his ultimate ability serves as a win condition with our infinite Chain Veil activations by double-ulting every opponent. Despite all of this, the main reason why he only became the better choice over [[card:Ugin, the Spirit Dragon]] recently is the Meta shift away from creature-based Midrange/Stax decks that play a slow, long game towards those types of decks also being able to finish a game quickly. In the old Meta, JTMS was disregarded as a "4-mana Sorcery-speed Brainstorm", but this is not the case anymore. He provides a wealth of utility and card advantage while also acting as a win condition, and being 2UU instead of 8 makes him much more castable than Ugin.

===endpanel

===panel: Budget Replacements

If you are interested in building a budget Teferi list, Dandelion/djmoneghan recently wrote an extensive post on Teferi lists for different budgets. You can read it [here.](https://www.reddit.com/r/CompetitiveEDH/comments/63vtwk/budget_deck_series_introduction_and_chain_veil/)

Not everyone (myself included) is made out of money, and sometimes you just have to look to alternatives to some of the pricier cards in the list. The following is a list of some of the most expensive cards in the list, and some alternative cards that may fill the same niche for less $$$.

- [[card: Grim Monolith]] and [[card: Mana Crypt]] - These should be the first card you get when building this deck. The deck *needs* these mana positive rocks, and it does not function anywhere near as well without them.

- [[card: Transmute Artifact]] - This card is essential to the deck, as it is the best way to get TCV. There are no budget replacements since the deck already runs all the possible similar effects.

- [[card: Power Artifact]] - This card is nowhere near essential, and [[card:Rings of Brighthearth]] already serves a similar purpose. Replace with whatever you think is appropriate for your local meta.

- [[card: Force of Will]] - [[card: Misdirection]] is almost functionally identical when you are trying to protect the combo.

- [[card: Mana Drain]] - [[card: Mana Leak]], [[card: Delay]] and [[card: Negate]] are all fine counterspells.

- [[card: Mox Diamond]] - No direct replacement, but if you think that you need a 0cmc artifact, there are [[card:Lotus Petal]] and [[card:Jeweled Amulet]]. Neither are recommended but both are somewhat playable. If you are struggling to get colored mana, then consider [[card:Coalition Relic]].

- [[card: Timetwister]] - The deck works fine without it. Just pick up something from the flex slots for your meta.

- [[card:The Tabernacle at Pendrell Vale]] - Just replace this with an Island and the deck will still function well. Now your [[card:High Tide]] is even better.

- [[card: Snapcaster Mage]] - Other than the [[card:Timetwister]] + [[card:Stroke Of Genius]] win condition Snapcaster doesn't serve a specific purpose in the deck. Just replace it with any other card.

===endpanel

===panel:Notable Exclusions

These are the cards that we think either do not belong in this type of build of Teferi, or are otherwise overshadowed by better cards we already run. Note that a card being listed here could still be a good card, the following are discussed *only* in the context of this exact Teferi build.


- [[card:Arcane Denial]]

You are in mono blue. There are better counterspells you could run. If you have this and not [[card:Delay]], make the switch ASAP. Only consider Denial if you are in a meta that is filled with cards that you can not hit with one of the deck's conditional counterspells.

- [[card:Capsize]]

Capsize is both bad at being a piece of removal and a combo finisher. Paying 1UU to bounce something is just never worth it, and you will pretty much never be able to cast it with buyback.

Capsize acts as an infinite mana outlet but it requires infinite U, which is not something that the deck can do easily. If the deck is at a point where it has infinite U then there are probably better lines you could take. In addition to that, bouncing the board does not technically win you the game, which can be relevant if you are playing with a timer.

- [[card:Coalition Relic]]

The card can act as a UU ritual, but ultimately the 3 CMC cost is a little bit too steep. The deck isn't too intensive on colored mana, and we want all our rocks to be as close as possible to either being mana positive or breaking even.

- [[card:Coldsteel Heart]]

Enters the battlefield tapped. The color is not worth it.

- [[card:Consecrated Sphinx]]

Consecrated Sphinx is a 6 mana do nothing spell. Sure, if you cast it you will win the next turn cycle, but that comes at a cost of not being able to interact with anything your opponents cast during that cycle. What that usually mean is that you lose the game.

Let's examine the other two 6cmc cards in the deck: Time Spiral and Teferi himself. These two cards pay for themselves, allowing you to hold up interaction, and they will win you the game either on the spot or during the next turn cycle. They do the same thing sphinx does but they don't cost you any of your resources.

Consecrated Sphinx also enables your opponent's removal. Being impervious to creature hate is one of Teferi's strong suits and giving your opponents a nice juicy removal target is just not something you want to do. That swords to plowshares could have killed a Selvala that was ready to win, or it could kill that [[card: Eidolon of Rhetoric]] that was preventing the deck from going off.

Mystic Remora is the final nail in the sphinx-shaped coffin. That card does the same thing as the sphinx for 1 mana. Your opponents paying 4 for their spells is just about as likely as them skipping their draw step in order to stop you from drawing cards with sphinx. This might be a radical opinion but I think that ancestral recall is a better card than a [[card:Minotaur Abomination]].

If you feel that your meta is slow enough for you to be able to run 6cmc card draw effects, look no further than [[card:Recurring Insight]]. Insight draws you 12-14 cards over a turn cycle, while the sphinx only draws 6.

- [[card:Recurring Insight]]

Speak of the devil. As I mentioned above, this card is not worth it unless you play in *incredibly* slow metas. 6 mana is just way too much to tap out for.

- [[card:Inspiring Statuary]]

Most of our instants are 1 or 2 CMC and ~60% of our artifacts are mana rocks. Being able to tap down our orbs on demand is cute but the card just does not do enough for the deck.

- [[card:Paradox Engine]]

This card is new, shiny and exciting, but it simply does not belong in the current build of the deck. In the current meta, the decks that are successful with paradox are the decks that can generate card advantage through it's untap triggers. Decks like Arcum, Sisay and some Thrasios builds all have a clear, guaranteed line to victory when you resolve paradox. The engine is inherently reliant on other cards you have in your hand, and that's simply not something that we want in this Teferi build.

If you want to play a mono-blue [[card:Paradox Engine]] general, then [Paradox Arcum](http://tappedout.net/mtg-decks/03-02-17-paradox-arcum/) is the deck you want.

- [[card:Long-Term Plans]]

The deck runs enough tutors to get us any card we want. [[card:Long-Term Plans]] is one of the worst tutors in blue and there are no notable otherwise non-tutorable cards in the deck.

In the current core build the only otherwise non-tutorable cards are: [[card:Trinket Mage]], [[card:Ugin, the Spirit Dragon]], [[card:Back To Basics]], [[card:Mystic Remora]] and [[card:Rhystic Study]], and none of them are critical enough to warrant an entire tutor slot dedicated to them.

- [[card:Jin-Gitaxias, Core Augur]]

Mono blue isn't big on reanimation, so you are going to be hardcasting this guy, and if you have that much mana then you could've probably already won the game.

- [[card:Intellectual Offering]]

The common argument for offering is that you target the weakest players, but at a competitive table *any* player should be able to mess your plans up once you give them such a huge advantage.

- [[card:Metalworker]]
- [[card:Staff of Domination]]

Metalworker is a meta call. In some metagames cursed totem is essential, and you simply cannot use metalworker early enough to get value out of it. If you can afford to run metalworker the deck will become more explosive.

Staff of Domination is not worth it whether or not you run metalworker. TCV is a robust combo and trying to shove more winconditions into the game will dilute it's consistency. Staff also does not interact with anything other than metalworker.

- [[card:Rising Waters]]

With the amount of tutors the deck has we can always get the best stax piece when we need it. There is no need to run strictly worse versions of them.

- [[card:Time Warp]]
- [[card:Savor The Moment]]

Time warp effects are often nothing other than 5 mana explores. Yes, they synergize with teferi, but if Teferi has landed on the battlefield you will either win the game on the spot or lock the game down. Time warps don't help our game plan in any meaningful way.

- [[card:Candelabra of Tawnos]]


Teferi is not a dedicated high tide deck, and thus it cannot interact with candelabra in any meaningful way. All it would do is filter colorless mana into U and that's just not something that the deck needs.

On tide turns generating 8 mana with Teferi alone should generally be enough to finish the game.

- [[card:Isochron Scepter]]
- [[card:Dramatic Reversal]]

The chain veil combo is already extremely consistent, and wasting two extra card slots on a combo we do not need is subpar at best. If the deck ran more meaningful 2CMC spells there would be merit to trying Scepter, but as of right now it's just lacking good targets.

The reason other top-tier decks run the iso-reversal combo is because it's either a solid, easily tutorable combo available to them (Jeleva) or because they have an easily accessible infinite mana outlet (Thrasios). Teferi is unique in that he has access to a combo that is simply **better** than iso-rev. Rather than dedicating three slots (reversal, isochron, outlet), the deck can afford to just run a one slot combo ([[card:The Chain Veil]]) and every possible tutor and piece of protection for it. You have your card draw, mana generation and an outlet all in one card.


- [[card:Sphere Of Resistance]]
- [[card:Lodestone Golem]]
- [[card:Thorn of Amethyst]]

[[card:Sphere of Resistance]] is a card commonly played in stax decks, but that alone is not a reason to run it. Sphere of resistance is anti-synergistic with Teferi, and it hurts us just as much as it hurts others. The only reason winter orb and it's kin are ran is because Teferi makes their downsides negligible.

Spheres also have the problem of making our mana-positive mana rocks significantly worse, and they force us to leave up more mana for interaction. Making our counterspells into cancels is not a place where we want to be.

[[card:Lodestone Golem]] synergizes with the deck moreso than the Sphere, but it still interacts unfavorably with our instants and 4CMC is a fairly high cost.

[[card:Thorn of Amethyst]] is generally ran in decks that have other creatures that support it and/or make it's tax less punishing. Since our deck is pretty much entirely non-creature spells this card is a big nonbo.


- [[card:Tidespout Tyrant]]

8 mana with triple U is an insane investment. It wins you the game, but it's just about the most inefficient way of doing so. In addition to that, the deck has no way to tutor it out so a lot of the time you are either not going to see it at all or it is going to be a dead draw.

- [[card:Maze of Ith]]
- [[card:Reliquary Tower]]
- [[card:Urborg]]

Utility lands are generally not worth running unless they have a good synergy with the deck. Every non-basic land we play makes our high tide worse, and the upside they provide is usually not significant enough. Running Urborg to compensate for running utility lands is in many ways akin to trying to put a bandage on a severed head.

- [[card:Void Winnower]]

This is not a reanimator deck. Guess some people missed the memo.

- [[card:Walking Ballista]]

Unlike JTMS/Ugin and [[card:Stroke of Genius]] are ran in the deck because they provide utility outside of the combo turn. While [[card:Walking Ballista]] can be used as a (very inefficient) [[card:Fireball]], it simply does not provide the same level of utility. In addition to that [[card:Walking Ballista]] gets shut down by our own [[card:Cursed Totem]], which is one of the stax pieces that gets tutored out almost every game.


- [[card:Worn Powerstone]]

It's mana negative (produces less than it costs) and it comes into play tapped. Avoid at all costs.

- [[card:Static Orb]]

The issue with Static Orb is that it's extremely hard to break parity on. Effects like Winter Orb are good because they only deny one type of resource (land), so our deck circumvents the loss of that resource (with artifact mana). Ultimately, [[card:Static Orb]] is just a little bit too awkward for the deck, as you *need* to have Teferi out in order to fully abuse it.

- [[card:Overburden]]

[[card:Overburden]] often ends up helping the decks that it's meant to hurt. In a vacuum it sounds fantastic. You drop it during the early game, and then every mana dork cast by our opponents ends up not netting them any mana. In practice, however, things are not that peachy. A smart green player can use [[card:Overburden]] to keep bouncing and replaying [[card:Gaea's Cradle]], netting them an absurd amount of mana. The card also does not favorably interact with our own stax pieces, as it lets our opponents circumvent the effects of cards such as [[card:Back To Basics]] and [[card:Winter Orb]].


- [[card:Mishra's Workshop]]

Sometimes this leads to explosive plays that win you the game extremely early. Other times you just wish it was a basic island. Note that the card only lets you *cast* artifact spells, and not use artifact abilities. The card is overall counterproductive to the deck's control plan, and as such it does not belong in a Teferi build that is not all in on stax/fast artifact combo.

===endpanel
===endaccordion


===endpanel



===panel:Matchups

The following is just a quick overview of some of the most popular decks in the format based on our collective experiences. These shouldn't be held as dogma, but they could provide some helpful advice if you are completely unfamiliar with the matchup.

===accordion

===panel:Blood Pod

**Gameplan**
Blood Pod is a creature-based Stax deck that, rather than completely locking down the table, seeks to use its Stax pieces as a way to gain tempo while accruing card advantage through Tymna and eventually winning the game through either raw creature damage or a Kiki-Jiki Combo.

**Weaknesses**

- [[card:Static Orb]], [[card:Stasis]] – Blood Pod tends to be fairly mana-hungry, so not letting them untap their recources will severely hamstring their development.
- [[card:Back to Basics]] - Even if the deck plays Blood Moon, it can still be caught off-guard by a well-timed Back to Basics.
- [[card:Cursed Totem]] – On top of using Mana dorks, Cursed Totem stops Yisan and and Kiki-Jiki Combo.
- [[card:Silent Arbiter]] – Blood Pod's main engine is Tymna draws, and Silent Arbiter stops those.
- Grafdigger's Cage/Tormod's Crypt – Blood Pod has a reanimator subtheme, and Cage also stops Yisan and Birthing Pod.
- [[card:Torpor Orb]] – This also stops the Kiki Combo and a few other cards like Eternal Witness.

**Notable Cards**

- Birthing Pod/Yisan – These two cards can both serve as a toolboxy value engine and as a way to assemble the Kiki combo.
- Null Rod/Stony Silence – Since our deck is heavily artifact-based, dealing with these is usually our topmost priority.
- Rule of Law/Eidolon of Rhetoric(/Ethersworn Canonist) – We are not able to combo off through a RoL effect, so they need to be removed before we go for the win. Oftentimes, we also want to play multiple spells in a turn to properly advance our board and hand, which isn't possible. Canonist is a slight exception to that rule since it still lets us play out our artifact ramp and hate pieces.
- Thalia, Guardian of Thraben/Sphere of Resistence – Sphere effects make it a lot harder for us to go off and will slow us down by a lot. We can play under them better than the fast combo decks, but they should be taken care off before we try to win.
- Root Maze – This card stops us from chaining mana rocks and makes having Fetchlands a miserable experience. It also makes The Chain Veil enter tapped, which can be very awkward.

===endpanel

===panel:Breakfast Hulk

**Gameplan**

Breakfast Hulk is the fastest deck in cEDH. Its speed can be problematic for us if we don't find the appropriate hate pieces early or leave up Countermagic from Turn 2 onwards. It usually tries to play either Hermit Druid or Flash + Protean Hulk to mill its whole library and reanimate a Laboratory Maniac for the win. If Hulk is running rampant in your Meta, slotting in cheap Countermagic like Dispel can help. Note that [[card:Tormod's Crypt]] does not stop the main combo, because [[card:Grand Abolisher]] in the pile.

**Weaknesses**

- Back to Basics – Hermit Druid's inherent weakness is not being able to run basic lands, and Back to Basics exploits that wonderfully.
- Grafdigger's Cage – This stops, Green Sun's Zenith, Reanimation, and all the Flashback spells.
- Cursed Totem – Hermit Druid needs to be activated, and the Cephalid Breakfast combo also relies on Nomads en-Kor's activated ability.

**Notable cards**

- Hermit Druid
- Flash
- Creature tutors

===endpanel


===panel:General Tazri

**Gameplan**

FCT is a fast combo deck that aims to win by acquiring infinite creature mana with Food Chain + Eternal Scourge/Misthollow Griffin and casting Tazri an infinite number of times. The deck is extremely fast and resilient, using the best ramp and interaction from all the 5 colors of magic.

**Weaknesses**

- [[card:Torpor Orb]] - Shuts down Tazri's ability making the combo impossible to execute.
- [[card:Back To Basics]] - Tazri is a 5 color deck, and because of that it is weak to non basic hate.
- [[card:Cursed Totem]] - While none of the actual combo pieces rely on creature activated abilities, the deck runs a fair number of dorks and a well placed cursed totem can slow the deck down by a few turns.

**Notable Cards**

- [[card:Food Chain]] - Try to stop this card at any cost.  Once it resolves it's usually a game over.
- [[card:Eternal Scourge]], [[card:Misthollow Griffin]] - The food chain fodder. Our deck does not have a clean way to interact with these so you should focus on stopping [[card:Food Chain]] instead. If the FCT player's mana is tight you could try bouncing [[card:Food Chain]] in response to [[card:Misthollow Griffin]] being played to save yourself a turn. This doesn't work with Scourge though, as it gets exiled when targeted.
- [[card:Tainted Pact]], [[card:Demonic Consultation]], [[card:Plunge Into Darkness]] - These cards usually get the FCT player food chain, while also exiling Scourge/Griffin on it's way down.
- [[card:Hagra Diabolist]], [[card:Halimar Excavator]], [[card:Kalastria Healer]] - these are the win condition of the deck. Keep note of how many of them get exiled when the FCT player [[card:Tainted Pact]]'s, because then stopping the ones cast during the combo may become relevant.

===endpanel

===panel:Jeleva, Nephalia's Scourge

**Gameplan**

Jeleva is a fast storm deck that runs a large amount of interaction and multiple redundant win conditions.

**Weaknesses**

The approach of the deck varies from game to game, and because of that it does not have any glaring weaknesses. In order to stop the deck you have to be aware of what win condition the Jeleva player is trying to reach, and try to counteract that. In general, try to see which resource the deck is struggling with the most and try to deny them that resource. Don't let the deck wheel when they only have a few cards in hand, and don't let them cast a [[card:Snap]] on high tide turns when they don't have a lot of mana open.

- [[card:Back To Basics]]
- [[card:Grafdigger's Cage]] - Shuts down [[card:Yawgmoth's Will]] and similar effects.

**Notable Cards**

- [[card:Candelabra of Tawnos]] - Helps the deck storm off on high tide turns.
- [[card:Doomsday]] - The doomsday piles of this deck usually pack a large number of protection so stopping it after doomsday resolves may be difficult. In general you want to either try to stop the pile from opening by countering the first draw spell cast after DD resolves, or you want to remove [[card:Laboratory Maniac]] if you think you have enough disruption to fight through protection.
- [[card:Helm of Awakening]] - In addition to just making storming off way easier for the Jeleva player, this card acts as a part of a combo with [[card:Future Sight]] and [[card:Sensei's Divining Top]].
- [[card:Isochron Scepter]] - Generates infinite mana with [[card:Dramatic Reversal]] and rocks.

===endpanel

===panel:Prossh, Skyraider of Kher

**Gameplan**

Prossh is in many ways similar to Tazri, trading the extra protection provided by the 5 colors for the easier execution of the combo and an extra burst of speed.

**Weaknesses**

Note that Prossh triggers on cast, and not on ETB, so the combo is not stopped by [[card:Torpor Orb]]

- [[card:Cursed Totem]] - The deck is reliant on mana dorks and [[card:Cursed Totem]] is our best way to punish that.

- [[card:Torpor Orb]] - Shuts down some of Prossh's win conditions. They can still win through [[card:Blood Artist]] effects or combat damage, so it's not a guaranteed lock.

**Notable Cards**

- [[card:Food Chain]] - The FCT advice applies here. Stop this at any cost.

- [[card:Vexing Shusher]], [[card:Guttural Response]], [[card:Pyroblast]], [[card:Red Elemental Blast]], [[card:Autumn's Veil]] - Prossh's stack interaction suite. Unfortunately for us, all these are really good against mono-blue.

===endpanel

===panel:Teferi Stax

**Gameplan**

See this primer.

**Weaknesses**

The Teferi mirror match is weird. If you find yourself in a duel against another Teferi player, try to stop as much of their ramp as possible. The deck that can develop a board position with a stax piece first is generally the deck that wins.

**Notable Cards**

See: SCD

===endpanel

===panel:Thrasios/Tymna

As of now, there are two big Thrasios/Tymna lists going around: Doomtide and Buried Alive/Angel Combo. We'll talk about both separately.

**Gameplan - Doomtide**

Doomtide is a deck that is very good at switching between different win conditions on the fly, which makes it fairly hard to disrupt once it's already in a position to win. It runs Bomberman, Aetherflux Reservoir, and Laboratory Maniac as its main win conditions. Rather than letting them get to a position where they can win, this deck should be attacked by denying it as much early resource development as possible.

**Weaknesses - Doomtide**

- [[card:Back to Basics]] - Inherently weak to nonbasic hate because it runs 4 colours.
- [[card:Cursed Totem]] - Shuts off the Bomberman line.
- [[card:Static Orb]]/[[card:Stasis]] - This deck thrives on being good at developing both its board and its hand at the same time. However, an early lockdown effect makes this almost impossible.

**Notable cards - Doomtide**

- [[card:High Tide]]
- Wheel effects
- [[card:Candelabra of Tawnos]]

===endpanel

===panel:Rashmi Control

**Gameplan**

Rashmi is a glacially slow draw-go control deck that aims to use Rashmi's passive ability to turn interaction into card advantage. The deck runs a ludicrous amount of interaction so it shouldn't be fought on the stack unless you are confident that you have exhausted their resources.

**Weaknesses**

- [[card:Cursed Totem]] - The deck relies on mana dorks to gain mana advantage. While shutting them down will not disable the deck, it will give you a little breathing space.
- [[card:Winter Orb]] - Mana denial makes it difficult for Rashmi to keep tapping out for counterspells.
- [[card:Tormod's Crypt]] - Rashmi's win condition and a lot of her value pieces heavily rely on graveyard recursion, so a well timed graveyard exile can make it hard for the deck to gain advantage.

**Notable Cards**

- [[card:Enter the Infinite]] - The deck's win condition. If it resolves Rashmi will (obviously) win the game. Denying Rashmi mana with either creature or land hate should make it a little bit harder for them to gather the ETI mana.
- [[card:Seasons Past]] - The deck's win outlet and best value card. A resolved mid game Seasons Past will often give Rashmi a significant advantage.


===endpanel

===panel:Tasigur Control

**Gameplan**

Tasigur is a control deck that uses Tasigur's activated ability, reanimation and the BUG interaction suite in order to control the other decks at the table.

**Weaknesses**

- [[card:Cursed Totem]] - Cursed totem shuts down Tasigur's ability and all of Tasigur's mana dorks, making it hard for the deck to gain a mana and card advantage.
- [[card:Back to Basics]] - Tasigur runs a low number of basic lands making him a prime target for our BtB.
- [[card:Grafdigger's Cage]] - Stops Tasigur's reanimation and flashback spells

**Notable Cards**

- [[card:Seedborn Muse]] - Combined with Tasigur's ability this card lets the Tasigur player get an insane advantage over the other players at the table.
- [[card:Palinchron]] + [[card:Phantasmal Image]] - Tasigur's infinite mana combo.
- [[card:Animate Dead]] - Reanimating a [[card:Consecrated Sphinx]] or a [[card:Seedborn Muse]] will often help the Tasigur player secure the win.

===endpanel

===panel:BUG/Grixis/BUGR Twin/Reanimator

**Gameplan**

Both Grixis Twin/Reanimator and BUGR Reanimator follow a similar gameplan: Playing a slower, more controlling game while putting big reanimation targets or combo pieces in the graveyard through Loot and Entomb effects, and then winning by resolving a reanimation spell. The most common reanimation targets and combo pieces are as follows:


- Kess/Jeleva Twin: Kiki-Jiki (with Deceiver Exarch/Pestermite and Phyrexian Delver), Jin-Gitaxias
- BUGR Reanimator: Necrotic Ooze (with Devoted Druid + Morselhoarder), Jin-Gitaxias, Razaketh, Consecrated Sphinx, Worldgorger Dragon
- Inalla Reanimator: Wanderwine Prophets, Jin-Gitaxias, Sire of Insanity

While the BUG Reanimator lists (Lime_Blue's SBT Combo, Lilbrudder's Demon Tyrant, Monkeryz's Mimeo Reanimator) are hit by the same types of hate as their Grixis and 4-colour counterparts, they play a slightly different game. Here are the different pieces that these lists use:

- SBT Combo: Necrotic Ooze, Hermit Druid, Food Chain
- Demon Tyrant: Razaketh, Protean Hulk, Food Chain, Jin-Gitaxias
- Mimeo: Razaketh, Necrotic Ooze, Jin-Gitaxias

**Weaknesses**

- Grafdigger's Cage/Tormod's Crypt – since these decks' combos usually all revolve around the graveyard, finding a piece of early graveyard hate should be high on the priority list.
- Cursed Totem – The only (currently played) Reanimator combo that isn't stopped by Cursed Totem is Inalla + Wanderwine Prophets. Nooze, Kiki, Hulk, Razaketh, and even Worldgorger Dragon all rely on activated creature abilities at one point or another.
- Timetwister/Time Spiral – Another spicy piece of graveyard hate.
- Torpor Orb – Stops Kiki combos, Worldgorger, Wanderwine Prophets, and some Hulk combos. Also stops Sidisi's ETB trigger and therefore the deck's Food Chain outlet.
- Pongify/Rapid Hybridization – While single-target creature removal isn't part of the generic list, these two spells can be great swap-ins for a Reanimation-heavy Meta.
- Cyclonic Rift/Chain of Vapor/Into the Roil – Bouncing a big reanimated creature is usually going to make the Reanimator player feel very bad about life.


**Notable cards**

- Entomb/Buried Alive/Survival of the Fittest/Intuition – these will be used to put reanimation targets right into the graveyard.
- Reanimation spells
- Show and Tell
- Hermit Druid
- Food Chain + Eternal Scourge

===endpanel

===panel:Thrasios Scepter Storm

Thrasios Scepter Storm is a bit different from other Storm decks. While it can play a standard Storm turn to kill people with Aetherflux Reservoir, it most commonly uses its Storm turn to assemble Dramatic Reversal + Isochron Scepter or Paradox Engine + Isochron Scepter + an Instant to go infinite and win the game from there.

**Weaknesses**

As is the case with other Storm decks, Thrasios Scepter Storm is quite resilient and not particularly weak to most of the cards we play. The only exceptions are:

- Back to Basics
- Grafdigger's Cage/Tormod's Crypt
- Tangle Wire

**Notable cards**

- Isochron Scepter
- Thrasios, Triton Hero
- Paradox Engine
- Ad Nauseam
- Yawgmoth's Will

===endpanel

===panel:Varolz Hulk

**Gameplan**

Varolz Hulk is a fast [[card:Protean Hulk]] combo deck that uses [[card:Varolz, the Scar-Striped]] as a sacrifice outlet. The deck's protean pile of choice is a [[card:Mikaeus, the Unhallowed]] combined with a [[card:Walking Ballista]].

**Weaknesses**

- [[card:Cursed Totem]] - Shuts down the main combo, mana dorks and sacrifice outlets of the deck.
- [[card:Grafdigger's Cage]] - Stops the deck's main combo, reanimation and to-battlefield tutors like [[card:Natural Order]].
- [[card:Tormod's Crypt]] - Exiling walking ballista in response to the undying trigger will stop the deck's main combo.

**Notable Cards**

- [[card:Protean Hulk]] - The deck's win condition.
- [[card:Natural Order]], [[card:Pattern of Rebirth]], [[card:Animate Dead]] - Ways to cheat hulk into play.
- [[card:Thoughtseize]], [[card:Cabal Therapy]], [[card:Duress]] - The deck's main disruption spells.
===endpanel

===panel:Zur, The Enchanter

**Gameplan**

Zur is the boogeyman deck of the format. Incredibly fast and resilient, the deck tries to storm out as early as possible, with Zur tutoring out [[card: Necropotence]] as a way to draw more cards when things get hairy.

**Weaknesses**

A lot of the deck's power lies in the multiple ways it can approach every scenario. While that does mean that the deck has no glaring weaknesses, this does not mean that the deck is unbeatable. You'll just have to rely on your skill rather than a silver bullet.

- [[card:Back to Basics]] - The deck is 3 color and as such it gets hurt by non-basic hate.
- [[card:Into The Roil]]/[[card: Chain Of Vapor]] - If you see a Zur player go for a pile that lacks protection ([[card: Lion's Eye Diamond]] is a dead giveaway), then bouncing [[card: Laboratory Maniac]] in response to their final draw will knock them out of the game.
- Counterspells - We are one of the few decks that have enough interaction to fight a deck like Zur on the stack.

**Notable Cards**

- [[card:Necropotence]]
- [[card:Grasp of Fate]]
- [[card:Ad Nauseam]]
- [[card:Doomsday]]

===endpanel

===panel:The Gitrog Monster

**Gameplan**

Gitrog Monster is a dredge combo deck that uses Gitrog's triggered ability in order to generate card advantage and combo off.

**Weaknesses**

- [[card:Cursed Totem]] - Shuts down the deck's discard outlets.

- [[card:Timetwister]] - The best form of grave hate we get in this matchup. It's unlikely that a gitrog player will leave a dredge card in a yard for you to twister away, but if that happens then shuffling it away might be a good idea.

**Notable Cards**

- [[card:Dakmor Salvage]] - This and a discard outlet such as [[card:Putrid Imp]] is a game over. I don't think the word limit of this post will let me explain the combo in detail so you'll just have to either trust me on this or look up a gitrog primer. The combo is nigh-impossible to disrupt so attack the discard outlets and gitrog before they get a chance to play this.

- [[card:Putrid Imp]], [[card:Wild Mongrel]], [[card:Noose Constrictor]], [[card:Oblivion Crown]] - The second half of the combo. Do not let these be on the battlefield at the same time as Gitrog.

===endpanel

===panel:Sidisi, Undead Vizer

**Gameplan**

Sidisi ANT is a deck that uses fast mana and Sidisi's tutor ability in order to power out an early [[card:Ad Nauseum]].

**Weaknesses**

- Counterspells - Being mono black the deck lacks good protection. Countering Ad Nauseum will usually do the trick.
- [[card: Chain of Vapor]]/[[card: Into The Roil]] - For exploit to work Sidisi has to be on the battlefield. Bouncing her back fizzles the exploit trigger, making the Sidisi player lose a turn and some mana.
- [[card:Timetwister]], [[card:Windfall]] - Sidisi *will* get Ad Nauseum. Making the ANT player discard it can slow them down for a considerate amount of time.
- [[card:Grafdigger's Cage]] - Shuts down [[card:Yawgmoth's Will]]

**Notable Cards**

- [[card:Ad Nauseam]] - If this resolves they win. The deck runs enough mana and recursion in order to cast and protect an [[card: Aetherflux Reservoir]]

- [[card:Aetherflux Reservoir]]/[[card:Tendrils Of Agony]] - The deck's win condition. Flusterstorm is pretty much our only way to stop a Tendrils.

- [[card:Exsanguinate]] - The decks alternate win condition.

- [[card:Defense Grid]] - Sidisi's go-to way to protect AN from disruption.

- [[card:Boseiju, Who Shelters All]] - Their way of protecting Ad Nauseum from pesky blue mages such as ourselves.

===endpanel

===panel:Selvala, Heart Of The Wilds

**Gameplan**

Selvala is a blazing fast combo deck that aims to land an early Selvala and use her ability to win as early as turn 3.
The general gameplan is:

1. Play a mana dork turn 1.

2. Play Selvala turn 2.

3. Try to win turn 3.

**Weaknesses**

This deck is blazing fast but it is at it's core a glass cannon. Removing the early selvala is often enough to knock the deck out of the game.

- [[card:Cursed Totem]] - Practically shuts the entire deck down. Be ready for the [[card:Nature's Claim]].
- [[card:Torpor Orb]] - Shuts down the draw aspect of the combo.
- [[card:Overburden]] - While it's hard to play this early enough to actively hurt the deck, it can make the Selvala player lose all their lands on their combo turn, and when backed with disruption that can be enough to knock the deck out of the game entirely.

**Notable Cards**

- [[card:Phyrexian Dreadnought]], [[card:Lupine Prototype]], [[card:Briarhorn]], [[card:Ravaging Riftwurm]] - Creatures that make Selvala generate mana.
- [[card:Temur Sabertooth]] - Combo piece with [[card:Wirewood Symbiote]] and a myriad other things. Luckily for us it's stopped by cursed totem.
- [[card:Nature's Claim]], [[card:Natural State]]
- [[card:Autumn's Veil]] - The deck's only way to protect itself on the stack.
- [[card:Song of The Dryads]]
- [[card:Berserk]], [[card:Invigorate]], [[card:Might of Old Krosa]] - Turn creatures into rituals. These cards can also be used to knock us out of the game with combat damage if we have something like a [[card:Cursed Totem]] out.

===endpanel

===panel:Yisan, Wanderer Bard

**Gameplan**

Yisan is a midrange deck that aims to use it's general's ability to tutor out the perfect silver bullets to lock out the board.


**Weaknesses**

- [[card:Cursed Totem]] - Shuts down their general's ability and all their mana dorks, essentially shutting down their whole deck.
- [[card:Grafdigger's Cage]] - Shuts down Yisan's ability and [[card:Green Sun's Zenith]]
- [[card:Overburden]] - Makes them lose a land every Yisan activation and every time they play a creature.
- [[card:Torpor Orb]] - Shuts down [[card:Reclamation Sage]] and some of Yisan's win conditions.

**Notable Cards**

- [[card:Quirion Ranger]], [[card:Wirewood Symbiote]] - The engine that makes the deck tick. Try to misstep one if they get hardcast.
- [[card:Null Rod]] - Our kryptonite. If this lands our deck cannot do anything until we find a removal spell.
- [[card:Phyrexian Revoker]] - This usually lands on verse 2 naming Teferi, making comboing off impossible until we find a removal spell.
- [[card:Bane of Progress]], [[card:Nature's Claim]], [[card:Natural State]], [[card:Beast Within]] [[card: Caustic Caterpillar]]
- [[card:Trinisphere]], [[card:Winter Orb]], [[card:Tangle Wire]], [[card:Thorn of Amethyst]], [[card:Sphere of Resistance]] - Most of Yisan's stax pieces are pretty bad for us. Luckily enough our stax pieces are pretty bad for Yisan as well, and we can power them out a bit earlier.
- [[card:Seedborn Muse]]
- [[card:Priest of Titania]]

===endpanel
===endaccordion

===endpanel


===panel: FAQ

> **Q**: Why are you playing a control deck? Everyone knows that control isn't a viable strategy in cEDH.
>
> **A**: Teferi is *not* a control deck. It is a combo deck first, a stax deck second, and a control deck third. You shouldn't try to control three other decks, as that is not, and it has never been a valid cEDH strategy. Attempting to combo off while simultaneously trying to lock the other three decks out of the game is the path that you should be taking.
>
> **Q**: Is the tutor consistency good enough to get veil out early?
>
> **A**: Yes. While at a surface look it may seem like the deck only has 6 artifact tutors ([[card:Whir of Invention]], [[card:Tezzeret the Seeker]], [[card:Inventors' Fair]], [[card:Fabricate]], [[card:Reshape]] and [[card:Transmute Artifact]]), there are a few other lines that can get you there. [[card:Intuition]] for either three tutors, or [[card:The Chain Veil]] when you have [[card:Academy Ruins]] gets you there. [[card:Mystical Tutor]] gets you the most appropriate tutor. [[card:Trinket Mage]] gets you [[card:Expedition Map]], which in turn gets you [[card:Inventors' Fair]]. [[card:Muddle The Mixture]] gets you [[card:Transmute Artifact]]. [[card:Merchant Scroll]] gives you [[card:Whir of Invention]]. [[card:Spellseeker]] gives you all of your cheap tutors. Some of the lines are more viable then others, but you will always have options. It's unlikely that [[card:Timetwister]] effects or a [[card:Dig Through Time]] won't get you there.
>
> **Q**: Is Teferi fast enough to race combo deck X?
>
> **A**: Unless we are talking about a glass cannon deck like Selvala then the answer is yes. The deck essentially runs a mana doubler in the command zone, so turn 3-4 wins are not only possible, but are commonplace. 12UU may seem like a crazy cost to win the game, but fast mana and mass untap effects will get you there *much* faster than you think.
>
> **Q**: What if the chain veil gets exiled?
>
> **A**: It's not a favourable position to be in, but the deck can assemble Rings/Power Artifact + Monolith fairly easily. [[card:Stroke of Genius]] wins the game there.

===endpanel

===panel: Conclusion

All in all, CVT is a deck that's very fun to play. It also has a fair amount of depth and can be challenging both in terms of in-game execution and deckbuilding choices - correctly deciding when you're trying to Combo and when you should play the Stax game is one of the biggest factors in winning or losing the game. It's also the only deck of its kind in competitive EDH with its ability to flip-flop between Stax and Fast Combo, which leads to a unique gameplay experience that can keep you entertained for a long time. We hope you'll have both fun and success playing Chain Veil Teferi!

===endpanel

===panel: Sample Lists

# Sample Lists

-   [Lobster's (SirOzzsome's) List](http://tappedout.net/mtg-decks/pw-teferi-combostax/)
-   [Neosloth's (superstepa's) List](http://tappedout.net/mtg-decks/stax-teferi/)
-   [Morimus's List](http://tappedout.net/mtg-decks/teferis-metalworks/)

===endpanel

===panel: Teferi Discord

If you've got any questions, new spicy tech, or you simply want to talk to someone about Teferi, join us on [this](https://discord.gg/3Y6CDyf) Discord server.
===endpanel
===panel:Latest Changes

This section of the primer goes over the latest major changes to the core decklist.

# June 2018

## SCD

Added Spellseeker to the creature suite.

Moved Sleight of Hand to the main card draw suite.

Moved Snapcaster Mage to misc flex slots.

Moved Gilded Drake to misc flex slots.

Moved Winter Orb to stax flex slots.

Added Pongify to removal flex slots.

Removed the creatures panel under SCD. The reasoning behind the change is that the creatures current in the core slots are both tutors.

# March 2018

## SCD
Moved Negate into the main interaction suite

Moved Cyclonic Rift into the main interaction suite

Moved Manifold Insights into the main card draw suite

Moved Jace, Vryn's Prodigy to Flex Slots

Moved Seat of the Synod into Flex Slots

## Currently Testing

Added Pull From Tomorrow to Currently Testing

Added Sleight of Hand to Currently Testing

Added Gilded Drake to Currently Testing

# September 2017

## Gameplan
Updated the cEDH deck graph

### SCD

**Ugin and JTMS**

Moved JTMS to Flex and Ugin back to main. Tymna is too good.


**Intuition**

Added more piles (thank you, /u/gates_88 for providing a few extra piles)

**Walking Ballista**

Added to notable exclusions

## Matchups

**Removed**

- Karador

**Added**

- Blood Pod
- Breakfast Hulk
- Reanimator
- Thrasios Storm
- Rashmi
- Tasigur
- Varolz

# May 2017

## Static Orb

Moved to Notable Exclusions from SCD->Stax

## Overburden

Moved to Notable Exclusions from SCD->Stax.

## Fact Or Fiction

Moved to SCD->Card Draw from Flex Slots.

## Manifold Insights

Added to Curently Testing->Card Draw.

## Ugin, The Spirit Dragon

Moved to Flex Slots from SCD->Combo.

## Jace, The Mind Sculptor

Added to SCD->Combo.

## Mishra's Workshop

Moved to Notable Exclusions from SCD->Lands.

## Misc

Removed some outdated card references (RIP Leo).

===endpanel
===endaccordion
