# Introduction
Hey everyone, neo and Lobster are back with another post where we go over the current state and most recent changes for our Teferi primer (our personal lists and the primer can be seen [here](http://tappedout.net/mtg-decks/stax-teferi/) and [here](http://tappedout.net/mtg-decks/chain-veil-teferi/)). We're going to show you which slots we changed, why we changed them, and how we changed our approach to certain cards that used to be a part of this deck. This post will go over all the changes we've gone through since the last "State of the Deck" post, in addition to talking about how these changes represent the changes in the philosophy of the deck.

Without any further ado, here's the changelog for the last couple primer updates:

# Changes

## List of Changes

### March 2018

### SCD
Moved Negate into the main interaction suite

Moved Cyclonic Rift into the main interaction suite

Moved Manifold Insights into the main card draw suite

Moved Jace, Vryn's Prodigy to Flex Slots

Moved Seat of the Synod into Flex Slots

### June 2018

### SCD

Added Spellseeker to the creature suite.

Moved Sleight of Hand to the main card draw suite.

Moved Snapcaster Mage to misc flex slots.

Moved Gilded Drake to misc flex slots.

Moved Winter Orb to stax flex slots.

Added Pongify to removal flex slots.


Removed the creatures panel under SCD. The reasoning behind the change is that the creatures currently in the core slots are both tutors.


## Overview

## Snapcaster Mage/JVP (moved to flex slots)

These cuts align with what we talked about in our [last](https://www.reddit.com/r/CompetitiveEDH/comments/6a0p33/state_of_the_deck_chain_veil_teferi/) State of the deck post. Snapcaster Mage and JVP can potentially be anti-synergistic with our own stax pieces, so they were moved away from the core list in favor of cards that are useful in more circumstances (Negate, Sleight of Hand).

## Cyclonic Rift (added to the core list)

Rift was added back to the core list because of a resurgence of Tymna hatebear decks. When played against fast combo, rift's overload is mostly irrelevant, but when facing down a board flooded with hatebears rift gives us an easy way to stablilize and set up a win.

## Gilded Drake (added to flex slots)

We still don't have a clear favorite between Gilded Drake and Vedalken Shackles, however, having either is necessary because it helps the deck have a favorable matchup against decks with creature based card advantage engines and commander centric decks. In the past Gilded Drake was excluded from the deck because it stops Teferi from working as a parity breaker, but now that Teferi doesn't fill that role anymore (more on that below) drake's upsides heavily outweigh its downsides.


## Manifold Insights (added to the core list)

Insights is a card that lets the deck keep up with other card advantage engines in the format. While the quality of the cards received from manifold will never be great, it is a sure way to refill our hand with interaction.

## Spellseeker (added to the core list)

Spellseeker is a new card that simply adds to our tutor density. Getting High Tide or Transmute Artifact is already good enough and this card can do more.

## Pongify (added to flex slots)

Pongify is just an all around solid removal spell. It's not a part of the core list because it is less versatile than generic bounce spells, but if you are playing in a creature heavy meta and you need something gone, this is the spell for that.

## Winter Orb (moved to flex slots)

Winter Orb is a card that many feel is iconic to Teferi, but unfortunately the card just doesn't work with the direction we want to take the deck in. Ever since the partner commanders introduce the combat step to the format using Teferi as a parity breaker became more and more difficult our main way to break parity on worb is our mana rocks. Unfortuantely, since this is cEDH, mana rocks are hardly something that is unique to Teferi. Pretty much every other deck in the format is just as well equipped to break parity on winter orb as we are, with green decks getting even more tools to play around it, so often enough the card ends up hurting us more than it hurts the rest of the decks at the table.

In addition to that, winter orb directly stops us from executing our control game plan, which the deck has been leaning on more and more with each update.

While there are still reasons to play Winter Orb in creatureless and control heavy metagames, we feel that the card is simply not versatile enough to be included in the core list for the deck.


# What's to come:


## The Xerox Philosophy

So, let's start things off with an experimental Teferi list that was made a while back: http://tappedout.net/mtg-decks/xerox-teferi-experiment/

This build sought to eschew some of the smaller, corner-case synergies the Teferi deck had back then in favour of strengthening the consistency of our main strategic avenues. This was achieved by increasing the amount of card draw and card selection while cutting the worst redundant effects along with a few lands. In 60-card Magic, the concept of cutting lands for card selection is known as the Turbo Xerox principle, which you can read more about here: https://birdsofparadise-mtg.blogspot.com/2011/10/turbo-xerox-rule.html

Many modern-day decks are built upon this principle - Vintage Mentor/Grow decks, Legacy Delver, and Modern Grixis Death's Shadow for example - sometimes with the people working on those decks being unaware of its existence. Now, how does this relate to not just the concrete card slot changes we've been making, but also the conceptual paradigm shift the deck has undergone?

To answer this question, we first need to examine how the Turbo Xerox principle relates to EDH. Things aren't as clean-cut as removing 2 lands for every 4 cantrips we play, and this has two reasons:

     - Our deck has 99 cards, not 60
     - We have to make do with one-ofs instead of full playsets

The combination of these two factors makes it more difficult for an EDH deck to execute its game plan with the same consistency as the decks we mentioned earlier. To combat this inherent disadvantage, we apply the following two methods in deckbuilding to increase the number of "hits":

    1. Using tutors that will let us find our relevant pieces, e.g. Fabricate, Transmute Artifact to find The Chain Veil (and other pieces)
    2. Using cards with similar effects as the ones we want to see in most games, e.g. Negate and Delay as redundant Counterspells

However, these methods come with a significant downside: diminishing returns. Not every copy of a certain effect is going to be as strong as we'd like it to be. Cancel f.i. is never going to be good enough, no matter how many Counterspells we want in this deck. There are certain card power minima we need to keep in mind when putting together a competitive deck, like not using Counterspells that cost more than 2 mana. So when it comes to increasing the amount of redundant copies of a certain effect, we need to be careful not to fall below the threshold of competitive viability. Another negative side effect of having too many copies of a certain piece is that they will be stranded in our hand, effectively being "dead" cards, when we don't need them. In other words, cards that are only circumstantially useful will often prevent us from advancing our gameplan when we draw them at the wrong time.

The question becomes: How can we make sure to have consistent access to the pieces and effects we need, at the time we need them, without detracting from the deck's overall card quality?

This is where we can take some inspiration from the Turbo Xerox principle. Instead of putting in, let's say, a bad Counterspell (Arcane Denial), a more circumstantial hate piece (Torpor Orb), a land that only helps one other card in the deck (Seat of the Synod) or a convoluted and expensive tutor line (Expedition Map -> Inventors' Fair -> The Chain Veil), we fill the deck with more cheap, efficient card draw spells. This has several benefits:

    - Our non-cantrip cards will be stronger on average, which leads to higher overall card quality
    - More card selection makes it easier to find the cards we need in a given situation
    - Cantrips are rarely "dead" draws, as they let us continue advancing our gameplan
    - Card draw (and especially card selection) has lower diminishing returns than most other effects
    - The deck will be more consistent overall

That being said, adding a bunch of cantrips does not come without a price. The downside of this approach is that the deck becomes slightly slower overall. This trade-off is not something every EDH deck is going to be happy with, so this expanded Turbo Xerox principle will not be applicable to every EDH deck with Blue in its colour identity. This raises the question: Why does this method work for Chain Veil Teferi?

Let's take a brief look at some speed-defining characteristics of cEDH decks and how they apply to Teferi:

    1. How many pieces does the main combo require?

Apart from our Commander, we only need The Chain Veil, so only one specific card needs to be found before we can win.

    2. Is any additional setup required to make that combo work?

Yes, we need some permanents that can produce more than 1 mana at a time. 5 mana off 3 permanents is required to go off with the minimal amount of total mana required.

    3. How much mana does the main combo require?

The most expensive part of the combo is casting and activating The Chain Veil, which will cost at least 8 mana.

    4. How quickly can we find our combo piece with tutors?

Since Teferi only gets access to Blue, we don't get to run the good Black tutors. While we can compensate with an array of artifact tutors and small tutor chains, none of them are as effective as Demonic Tutor et al.

These questions show that by nature of its combo, Chain Veil Teferi is not going to be as consistently fast as other decks in the format. We can use this to our advantage by trading that bit of potential speed for all the advantages we mentioned earlier, which will leave us with a deck that's more consistent and powerful. One of Teferi's main strengths has always been its flexibility and the ability to pivot from one gameplan to another depending on what the situation calls for. Expanding on the deck's Xerox aspect further contributes to these strengths, thereby solidifying Teferi's new place in the cEDH Meta.

## Moving Away From Stax

Apart from Xerox-ifying Teferi, there's another big reason why we've been moving away from the deck's Stax angle. Many of the Stax pieces we used to run - like Winter Orb, Static Orb, and even things like Embargo if we're going way back - were reliant on us being able to utilise our Commander as a mana advantage engine to break parity through these effects and continue developing our gameplan at a faster pace than the other decks at the table. This means that a requirement for those pieces to function was being able to cast Teferi and keeping him alive for multiple turn cycles. Taking a brief look at the online cEDH meta shows that creature-based strategies are more popular than ever. Tymna is one of the most played Commanders right now, and she has a tendency to bring a whole entourage of creatures with her. Even the BUGW Scepter/Storm decks have started dipping more into mana dorks to better abuse Tymna's power as a card draw engine. On top of those, there's creature combo strategies such as Hulk, Food Chain, and Razaketh decks, and many slow strategies are based on the tried and tested Tymna + Hatebears formula (Blood Pod, Hulkweaver, Thras/Tymna Tap Stax, some MAN builds). All of this means that running out Teferi for 6 mana without being able to win on the same turn is just not worth it anymore. There will usually be too many creatures too early into the game for Teferi to even survive a single turn cycle after being played. And since value Teferi isn't viable anymore with the exception of corner cases, we'll be better off with putting in stronger general purpose cards to replace the Stax pieces that have lost their synergy with the deck due to this shift in both the cEDH meta at large and our approach to building Teferi.
