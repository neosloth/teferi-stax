# Introduction
Hey everyone, neo and Lobster are back with another post where we go over the current state and most recent changes for our Teferi deck (lists [here](http://tappedout.net/mtg-decks/stax-teferi/) and [here](http://tappedout.net/mtg-decks/chain-veil-teferi/)). We've noticed that people have been asking about the thought process and decision making that goes into card changes for major decklists, so we thought it might be good to provide some insight into how we do it for Teferi. We're going to show you which slots we changed, why we changed them, and how we changed our approach to certain cards that used to be a part of this deck. This is the second iteration of the "State of The Deck"

Without any further ado, here's the changelog:

# Changes
## List of Changes

* [[Static Orb]] -> [[Fact or Fiction]]
* [[Overburden]] -> [[Manifold Insights]]
* [[Ugin, the Sprit Dragon]] -> [[Jace, the Mind Sculptor]]
* [[Mishra's Workshop]] -> [[Island]]

# Mishra's Workshop

Mishra's Workshop seemed to be a perfect fit for this deck at first glance, since it has the potential to let us cast multiple mana rocks in one turn. However, from playing the deck in its most recent iteration, we found that it's not pulling its weight anymore. Here's why:

1. With the switch to more card advantage spells and JTMS as our finisher of choice, we have a significantly higher requirement for Blue mana compared to high amounts of generic Mana.

2. Workshop is often a dead card in our opening hand. Outside of Mana rock chains (which already somewhat facilitate themselves due to the high amount of mana-positive rocks we run), it helps us cast four cards early: Basalt Monolith, Gilded Lotus, Thran Dynamo, The Chain Veil. Since Chain Veil is kind of a nonbo with Workshop (it can't pay for its activation and doesn't help cast Teferi), this makes only three cards in the deck that are good to have in our opener together with Workshop.

3. The most important card we could cast with Workshop mana was Static Orb, which has recently been removed from the deck. Being able to cast Static Orb off tapping just one permanent was a big deal, but we don't need to be able to do that anymore.

4. Removing Workshop doesn't weaken our Expedition Map lines - Map gets either Tabernacle or Inventors' Fair most of the time, and if we need to get a "normal" land from it, Ancient Tomb is better in almost every case since it lets us cast any type of spell.

All of these factors combined made us move away from Mishra's Workshop, and unless the list changes in a big way, we will probably not put it back in.

# Conclusion

While on the surface this update may seem somewhat insignificant (after all, only 4 slots were affected), it represents a pretty major shift in the deck's philosophy. Teferi was always seen as the hardcore artifact stax deck, and we think that at the moment such a build of Teferi is not entirely viable. With the exception of a few inarguably incredible stax pieces such as [[Cursed Totem]], [[Winter Orb]] and [[Stasis]], the Stax cards mono-u has access to are all inherently buildaround and somewhat hard to set up. Having a card that twists the deck around it is not something we want to have in an adaptive deck like Teferi, so we think that it is more wise to instead expand on the deck's other gameplans.

# What's to come:

[[Protean Hulk]] was unbanned recently and has already spawned several strong lists. That being said, a lot of those are still in the testing stage and the influence of Hulk on the cEDH Meta hasn't really been established yet. We'll keep an eye on those decks and once the proper lists have been established, we'll update our Primer accordingly. However, don't expect a big change in the deck - we both believe that Hulk is much less threatening than people make it out to be, and Teferi already has all the tools to deal with all sorts of Hulk combos - Grafdigger's Cage, Cursed Totem, and Torpor Orb, to just name a few. This next update will also come with an update to the matchup graph and descriptions to more accurately reflect the current state of the cEDH Meta.


One quick bit of advice before we go back to working on the deck: Do NOT slot in [[Tormod's Crypt]] as a way to deal with Hulk - it will not help you. The (currently) good Hulk lines all use [[Grand Abolisher]], which means that we won't be able to activate Crypt at a point where it could stop them.


That's all for now! We hope you're all still having fun with the list, and we'll be back with more updates in the future.
